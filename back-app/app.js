var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var winston = require('winston'),
    expressWinston = require('express-winston');
//APP
var app = express();
// APP Use
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
//Config Html
app.use(express.static(__dirname + '/node_modules/jquery/dist'));
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));
app.use(express.static(__dirname + '/public'));
// Log request
app.use(expressWinston.logger({
    transports: [
        new winston.transports.File({ filename: 'logs/app.log', level: 'info' }),
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    ),
    meta: true,
    msg: function(req, res) { return `${res.statusCode} - ${req.method}` },
    expressFormat: true,
    colorize: false,
    ignoreRoute: function(req, res) { return false; }
}));

//routes
app.use(require('./routes/routes'));

// Log Errors
app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    ],
    msg: '{{err.message}}',
    level: function() {
        return 'warn';
    }
}));

// Start server
var server = app.listen(3000);

//export
module.exports = server;