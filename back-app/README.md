# Backend App

Descripción del ejercicio disponible en

https://www.dropbox.com/sh/gfqisikrhuslbu0/AADUE52toTZKPjM6AHmNrkKMa?dl=0

## Getting Started

### Prerequisites

	- Node Js v8.11.2

### Installing

	1 - Clonar el repositorio.
	2 - Ejecutar 'npm i' para instalar las dependencias (Node_modules).
	3 - Ejecutar 'node app' para iniciar el servidor.

## Documentation

### PDF File

Dentro de la carpeta principal se encuentra un documento llamado BackApp.pdf que contiene la documentación de la API Rest

### HTML

En la raíz también hay un archivo HTML que contiene documentación y formas de ejecutar la API. Se puede visualizar ingresando en http://localhost:3000/ 

### Authentication & Authorization

#### Authentication

Tanto Usuarios como Administradores deben tener un token para todos los servicios

#### Authorization

Usuario

  - Puede consultar usuarios por ID cuyo rol sea USER
  - Puede consultar usuarios por NOMBRE cuyo rol sea ADMIN

Administrador

  - Puede consultar usuarios por ID (Admin y User)
  - Puede consultar usuarios por Nombre (Admin y User)
  - Puede consultar Politicas asociadas a un usuario
  - Puede consultar Usuarios asociados a una política

## Running the tests

### API Rest

La Api contiene las siguientes pruebas:

  GET /api
    √ Not ID - Error - ID is Missing
    √ Incorrect ID - Error - Incorrect ID
    √ Correct ID - Get correct data

  GET /dataUser Error - Header is Missing
    √ Forbidden - /dataUser/id
    √ Forbidden - /dataUser/name

  GET /policies Error - Header is Missing
    √ Forbidden - /policies
    √ Forbidden - /policies/user

  GET /dataUser Error - Incorrect Token
    √ Forbidden - /dataUser/id
    √ Forbidden - /dataUser/name

  GET /policies Error - Incorrect Token
    √ Forbidden - /policies
    √ Forbidden - /policies/user

  GET /dataUser/ID
    √ Not ID - Error - ID is Missing
    √ Incorrect ID - Error - Incorrect ID
    √ Correct ID - Get Data
    √ User Role cannot access Admin data
    √ User Admin access Admin Data
    √ User Admin access User Data

  GET /dataUser/name
    √ Not Name - Error - Name is Missing"
    √ Incorrect Name - Error - User not found
    √ Correct Name - Get Data
    √ User Role cannot access Admin data
    √ User Admin access Admin Data
    √ User Admin access User Data

  GET /policies
    √ Not Id - Error - User ID is Missing
    √ Not User - Error - User not found
    √ Forbidden Access - Error - Acceso denegado
    √ Correct ID - Get Data (1703ms)

  GET /policies/user
    √ Not Id - Error -  ID is Missing
    √ Policies not found
    √ Forbidden Access - Error - Acceso denegado

Para realizar las pruebas, se debe ejecutar el comando 'mocha test'.

Además se puede probar la aplicación mediante POSTMAN, usando la siguiente colección:

https://www.getpostman.com/collections/12d430247f751cd4c179

## Built With

	- Node Js v8.11.2
	- Express 4.16.0


## Authors

Devoto Federico 

