exports.createResponse = function(res, status, success, message) {
    return res
        .status(status)
        .send({ success: success, message: message });

};