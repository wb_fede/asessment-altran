const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config/config');

//controller
const userController = require('../controllers/users');

//Service
const responseService = require('../services/response');
const mockyService = require('../services/mocky');

// without certificate
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const type = 'clients';
//Authenticate and Authorization 
exports.createToken = function(req, res) {
    //need id
    if (!req.query.id) {
        return responseService.createResponse(res, 404, false, "Error - ID is Missing");
    }
    //Get data Filtered by id
    let options = { id: req.query.id };
    mockyService.getData(config.URL_CLIENT, options, type).then((data) => {
        //get count
        let count = Object.keys(data).length;
        if (count > 0) {
            let role = data[0].role;
            let payloadSub = req.query.id + ' ' + role;

            //generate token
            let payload = {
                sub: payloadSub,
                iat: moment().unix(),
                exp: moment().add(2, "hours").unix(),
            };
            // return the information including token as JSON
            return responseService.createResponse(res, 200, true, jwt.encode(payload, config.TOKEN_SECRET));

        } else { //Incorrect ID
            return responseService.createResponse(res, 404, false, "Error - Incorrect ID");
        }
    }, (error) => {
        return responseService.createResponse(res, 500, false, "Error with user service");
    });

};

// Middleware
exports.ensureAuthenticated = function(req, res, next) {
    if (!req.headers.authorization) {
        return responseService.createResponse(res, 403, false, "Error - Headers missing");
    }

    try {
        let token = req.headers.authorization.split(" ")[1];
        let bearer = req.headers.authorization.split(" ")[0];
        let payload = jwt.decode(token, config.TOKEN_SECRET);

        if (bearer != 'Bearer') {
            return responseService.createResponse(res, 403, false, "Error - Incorrect Bearer");
        }
        try {
            if (payload.exp <= moment().unix()) {
                return responseService.createResponse(res, 401, false, "Error - The token expires");
            }

            req.user = payload.sub;
            next();
        } catch (error) {
            return responseService.createResponse(res, 403, false, "Error - Incorrect Token");
        }
    } catch (error) {
        return responseService.createResponse(res, 403, false, "Error - Incorrect Token");
    }


}

exports.ensureAuthorization = function(req, res, next) {
    try {
        let role = module.exports.getRole(req);
        if (role != 'admin') {
            return responseService.createResponse(res, 403, false, "Acceso Denegado");
        }
        next();
    } catch (e) {
        console.log(e);
        return responseService.createResponse(res, 403, false, "Acceso Denegado");
    }
}

exports.getRole = function(req) {
    try {
        let token = req.headers.authorization.split(" ")[1];
        let payload = jwt.decode(token, config.TOKEN_SECRET);
        let role = payload.sub.split(" ")[1];
        return role;
    } catch (e) {
        return null;
    }
}