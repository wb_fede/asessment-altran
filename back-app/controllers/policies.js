const https = require('https');
const _ = require("underscore");
const jwt = require('jwt-simple');
const config = require('../config/config');

// without certificate
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'

//Services
const responseService = require('../services/response');
const mockyService = require('../services/mocky');

// data
const type = 'policies';

//get policies from user id
exports.getPoliciesUser = function(req, res) {
    //check Id
    if (!req.query.id) {
        return responseService.createResponse(res, 403, false, "Error - ID is Missing");
    }

    //get data
    let options = { clientId: req.query.id };
    mockyService.getData(config.URL_POLICIES, options, type).then((data) => {
        const rdo = mockyService.handlerData(res, data, 'Client ID');
        return responseService.createResponse(res, rdo.status, rdo.success, rdo.message);
    }, (error) => {
        return responseService.createResponse(res, 500, false, 'Error Service');
    });

};


//get user by policie number
exports.getUserByPolicie = function(req, res) {

    //check Id
    if (!req.query.id) {
        return responseService.createResponse(res, 403, false, "Error - ID is Missing");
    }

    //get data
    let options = { id: req.query.id };
    mockyService.getData(config.URL_POLICIES, options, type).then((data) => {
        const rdo = mockyService.handlerData(res, data, 'ID');
        return responseService.createResponse(res, rdo.status, rdo.success, rdo.message);
    }, (error) => {
        return responseService.createResponse(res, 500, false, 'Error Service');
    });

};