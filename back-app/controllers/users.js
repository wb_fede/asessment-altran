const https = require('https');
const _ = require("underscore");
const jwt = require('jwt-simple');
const config = require('../config/config');

//Service
const responseService = require('../services/response');
const mockyService = require('../services/mocky');
const authService = require('../services/auth');

// without certificate
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'
    // Data
const type = 'clients';
// get list of users
exports.getUsersFilterID = function(req, res) {
    //check Id
    if (!req.query.id) {
        return responseService.createResponse(res, 403, false, "Error - ID is Missing");
    }

    // check role => User Role cannot see user Admin data
    const role = authService.getRole(req);
    let options;
    if (role != 'admin') {
        options = { id: req.query.id, role: role };
    } else {
        //get data
        options = { id: req.query.id };
    }

    mockyService.getData(config.URL_CLIENT, options, type).then((data) => {
        const rdo = mockyService.handlerData(res, data, 'ID');
        return responseService.createResponse(res, rdo.status, rdo.success, rdo.message);
    }, (error) => {
        return responseService.createResponse(res, 500, false, 'Error Service');
    });

};


exports.getUsersFilterName = function(req, res) {

    //check Id
    if (!req.query.name) {
        return responseService.createResponse(res, 403, false, "Error - Name is Missing");
    }

    // check role => User Role cannot see user Admin data
    const role = authService.getRole(req);
    let options;
    if (role != 'admin') {
        options = { name: req.query.name, role: role };
    } else {
        //get data
        options = { name: req.query.name };
    }


    mockyService.getData(config.URL_CLIENT, options, type).then((data) => {
        const rdo = mockyService.handlerData(res, data, 'Name');
        return responseService.createResponse(res, rdo.status, rdo.success, rdo.message);
    }, (error) => {
        return responseService.createResponse(res, 500, false, 'Error Service');
    });

};