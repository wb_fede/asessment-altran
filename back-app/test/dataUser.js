const request = require('supertest');
const app = require('../app');
let token = '';
let tokenAdmin = '';
const idAdmin = 'e8fd159b-57c4-4d36-9bd7-a59ca13057bb';
const idUser = 'a3b8d425-2b60-4ad7-becc-bedf2ef860bd';
const nameUser = 'Barnett';
const nameAdmin = 'Britney';

before(
    function() {
        let user = request(app)
            .get('/api')
            .query({ id: idUser })
            .end(function(err, res) {
                token = 'Bearer ' + res.body.message;
            });
        let userAdmin = request(app)
            .get('/api')
            .query({ id: idAdmin })
            .end(function(err, res) {
                tokenAdmin = 'Bearer ' + res.body.message;
            });
    }
);
// DATA USER ID
describe('GET /dataUser/ID', function() {
    this.timeout(10000);
    it('Not ID - Error - ID is Missing', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('message' in res.body) || !(res.body.message.includes('ID'))) throw new Error("Incorrect Message")
                if (!('success' in res.body) || (res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    it('Incorrect ID - Error - Incorrect ID', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: 'TEST' })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('message' in res.body) || !(res.body.message.includes('ID'))) throw new Error("Incorrect Message")
                if (!('success' in res.body) || (res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    it('Correct ID - Get Data', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: idUser })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Missing message key")
                if (!('success' in res.body)) throw new Error("Missing success key")
                if (!('name' in res.body.message[0])) throw new Error("Missing name key")
                if (!('email' in res.body.message[0])) throw new Error("Missing email key")
                if (!('role' in res.body.message[0])) throw new Error("Missing role key")
            })
            .end(done)
    });

    // Check Roles. User Role => cannot access  Admin Users
    it('User Role cannot access Admin data', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: idAdmin })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('message' in res.body) || !(res.body.message.includes('Incorrect'))) throw new Error("Incorrect Message")
                if (!('success' in res.body) || (res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    // Admin
    it('User Admin access Admin Data', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: idAdmin })
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Incorrect Message")
                if (!('success' in res.body) || !(res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    it('User Admin access User Data', function(done) {
        request(app)
            .get('/dataUser/id')
            .set('Accept', 'application/json')
            .query({ id: idUser })
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Incorrect Message")
                if (!('success' in res.body) || !(res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });
});

//DATA USER NAME
describe('GET /dataUser/name', function() {
    this.timeout(10000);
    it('Not Name - Error - Name is Missing"', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(403)
            .expect(function(res) {
                if (!('message' in res.body) || !(res.body.message.includes('Name'))) throw new Error("Incorrect Message")
                if (!('success' in res.body) || (res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    it('Incorrect Name - Error - User not found', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: 'TEST' })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('message' in res.body) || !(res.body.message.includes('Name'))) throw new Error("Incorrect Message")
                if (!('success' in res.body) || (res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    it('Correct Name - Get Data', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: nameUser })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Missing message key")
                if (!('success' in res.body)) throw new Error("Missing success key")
                if (!('name' in res.body.message[0])) throw new Error("Missing name key")
                if (!('email' in res.body.message[0])) throw new Error("Missing email key")
                if (!('role' in res.body.message[0])) throw new Error("Missing role key")
            })
            .end(done)
    });

    // Check Roles. User Role => cannot acces Admin Users
    it('User Role cannot access Admin data', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: nameAdmin })
            .set('Authorization', token)
            .expect('Content-Type', /json/)
            .expect(404)
            .expect(function(res) {
                if (!('message' in res.body) || !(res.body.message.includes('Incorrect'))) throw new Error("Incorrect Message")
                if (!('success' in res.body) || (res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    // Admin
    it('User Admin access Admin Data', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: nameAdmin })
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Incorrect Message")
                if (!('success' in res.body) || !(res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

    it('User Admin access User Data', function(done) {
        request(app)
            .get('/dataUser/name')
            .set('Accept', 'application/json')
            .query({ name: nameUser })
            .set('Authorization', tokenAdmin)
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(function(res) {
                if (!('message' in res.body)) throw new Error("Incorrect Message")
                if (!('success' in res.body) || !(res.body.success)) throw new Error("Incorrect Success")
            })
            .end(done)
    });

});