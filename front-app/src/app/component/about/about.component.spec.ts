import { TestBed, ComponentFixture } from '@angular/core/testing';
import { AboutComponent } from './about.component';
describe('AboutComponent', () => {
    let component: AboutComponent;
    let fixture: ComponentFixture<AboutComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AboutComponent]
        });

        fixture = TestBed.createComponent(AboutComponent);
        component = fixture.componentInstance;
    });

    it('Debe de crearse el componente', () => {
        expect(component).toBeTruthy();
    });
});
