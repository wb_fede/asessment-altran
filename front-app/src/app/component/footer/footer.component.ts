/*
Component: About Component
Description: Component of the footer
Author: Devoto federico
*/
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
  public copyright;
  constructor() {
     this.copyright = new Date();
  }

  ngOnInit() {
  }

}
