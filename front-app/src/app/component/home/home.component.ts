/*
Component: About Component
Description: Component of the Home view
Author: Devoto federico
*/
import { Component, OnInit, HostListener, Inject,  } from '@angular/core';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})


export class HomeComponent implements OnInit {
  isScrolled = false;
  constructor(@Inject(DOCUMENT) private document: any) {

   }

  ngOnInit() {

  }

  @HostListener('window:scroll', [])
        onWindowScroll() {
          const number =  this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
          if (number > 200) {
            this.isScrolled = true;
          } else if (this.isScrolled && number < 10) {
            this.isScrolled = false;
          }
    }

  top() {
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
  }

}
