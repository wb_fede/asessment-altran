import { SearchComponent } from './search.component';

describe('SearchComponent - Unit', () => {
    let component: SearchComponent;
    // init component
    beforeEach( () => {
        component = new SearchComponent(null, null, null);
    });
    it('Debe de crearse el componente', () => {
        expect(component).toBeTruthy();
    });
    // function changeProfession
    it('changeProfession: Si recibe un cambio, debe guardarlo en profFilter', () => {
        // init
        const profFilter = {
            value : 'Test'
        };
        component.changeProfession(profFilter);
        // check path
        expect(component.profFilter).toEqual(profFilter.value);
    });
    // function changeProfession
    it('changeFriend: Si recibe un cambio, debe guardarlo en friendFilter', () => {
        // init
        const friendFilter = {
            value : 'Test'
        };
        component.changeFriend(friendFilter);
        // check path
        expect(component.friendFilter).toEqual(friendFilter.value);
    });
})