import { SearchComponent } from './search.component';
import { BrastlewarkDataService } from '../../services/brastlewark-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { async, TestBed, tick, inject } from '@angular/core/testing';
import { throwError, of } from 'rxjs';
import { FilterPipe } from '../../pipes/filter.pipe';


describe('SearchComponent', () => {
    let component: SearchComponent;
    const service = new BrastlewarkDataService(null);
    const router = {
        navigate: jasmine.createSpy('navigate')
      };
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SearchComponent, FilterPipe],
            providers: [
                { provide: Router, useValue: router },
                { provide: ActivatedRoute, useValue: { params: of({prof: 'All', friend: 'All', name: 'All'})}},
                {provide: BrastlewarkDataService, useValue: service},
            ]
        })
        // the below was added
        .compileComponents().then(() => {
            const search = TestBed.createComponent(SearchComponent);

            component = search.componentInstance;
        });
    }));
    // Unit Tests
    it('NgOnInit - Service: Si hay un error en el servicio, debe no mostrar filtros', () => {
        // spy
        spyOn( service, 'getBrastlewarkData').and.callFake(() => {
            return throwError(new Error('Fake error'));
          });

        component.ngOnInit();
        // check message and flags
        expect( component.filtersData).toBe(null);
    });
    // carga de filtros
    it('NgOnInit - Filters: Carga de Filtros', () => {
        // service
        const jsonFake = {
            'Brastlewark': []
        };
         // spy
         spyOn( service, 'getBrastlewarkData').and.returnValue(of(jsonFake));
        // init
        component.ngOnInit();
        // check filter
        expect( component.profFilter).toBe('All');
        expect( component.friendFilter).toBe('All');
    });
    // carga de Data Filters
    it('NgOnInit - Filters: Deben cargarse los datos para los filtros, si el servicio devuelve información', () => {
        // service
        const jsonFake = {
            'Brastlewark': []
        };
         // spy
         spyOn( service, 'getBrastlewarkData').and.returnValue(of(jsonFake));
        // init
        component.ngOnInit();
        // check length
        expect( component.filtersData.length).toBe(0);
    });
    // function Filter and route
    it('filter: Si no recibe nombre, debe colocar All en el route', () => {
        // init
        component.filter(null);
        const url = router.navigate.calls.first().args[0];
        // check path
        expect(url).toEqual([ '/home', 'All', 'All', 'All' ]);
    });
    // function Filter and route
    it('filter: Si recibe nombre, debe colocar el valor en el route', () => {
        // init
        const nombre = {
            value : 'Test'
        };
        router.navigate.calls.reset();
        component.filter(nombre);
        const url = router.navigate.calls.first().args[0];
        // check path
        expect(url).toEqual(['/home', nombre.value, 'All', 'All' ]);
    });
});
