/*
Component: About Component
Description: Component of the loader to Grid
Author: Devoto federico
*/
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html'
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
