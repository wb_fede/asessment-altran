/**
 * Creator: Devoto Federico
 * Description: service to get information about Brastlewark habitants. This service provide information to table view or
 * cards grid.
 * HttpClient: this class is used to get REST information from the URL. This URL is defined in environment.ts.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


// Decorator
@Injectable({
  providedIn: 'root'
})
// Class Definition
export class BrastlewarkDataService {

  // get Api URL
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  // function to get information from url
  public getBrastlewarkData() {
    return this.http.get(this.apiUrl);
  }

}
