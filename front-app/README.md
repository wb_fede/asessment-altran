# FrontApp

Descripción del ejercicio disponible en

https://www.dropbox.com/sh/gfqisikrhuslbu0/AADUE52toTZKPjM6AHmNrkKMa?dl=0

## Getting Started

### Prerequisites

	-Angular CLI: 6.0.8
	-Node: 8.11.2
	-Angular: 6.0.4

### Installing

	1 - Clonar el repositorio.
	2 - Ejecutar 'npm i' para instalar las dependencias (Node_modules).
	3 - Ejecutar 'ng serve' para iniciar el servidor.

## Documentation

### PDF File

Dentro de la carpeta principal se encuentra un documento llamado Frontapp.pdf que contiene la documentación de la APP

### Compodoc

Ejecutar 'node_modules/.bin/compodoc -s -d docs'. Luego navegar a 'http://127.0.0.1:8080'

## Development server

Ejecutar `ng serve`. Luego navegar a  `http://localhost:4200/`. 

## Running the tests

### API Rest

La Api contiene las siguientes pruebas:

	AboutComponent
		Debe de crearse el componente
	footerComponent
		Debe de crearse el componente
	GridComponent
		NgOnInit - Service: Si hay un error en el servicio, debe mostrar el mensaje
		NgOnInit - Excepcion route: debe mostrar mensaje
		NgOnInit - Sin datos: Si no hay datos, debe mostrar mensaje
		NgOnInit - Filter: filtro de Nombre, debe retornar 1 habitante
		NgOnInit - Filter: filtro de Profesión, debe retornar 1 habitante
		NgOnInit - Filter: filtro de Amigos, debe retornar 2 habitantes
		NgOnInit - Filter: Todos los filtros con valor, debe retornar 0 habitantes
	GridComponent - Unit
		Debe de crearse el componente
	HomeComponent
		Debe de crearse el componente
	LoaderComponent
		Debe de crearse el componente
	NavbarComponent
		Debe de crearse el componente
	SearchComponent
		NgOnInit - Service: Si hay un error en el servicio, debe no mostrar filtros
		NgOnInit - Filters: Carga de Filtros
		NgOnInit - Filters: Deben cargarse los datos para los filtros, si el servicio devuelve información
		filter: Si no recibe nombre, debe colocar All en el route
		filter: Si recibe nombre, debe colocar el valor en el route
	SearchComponent - Unit
		Debe de crearse el componente
		changeProfession: Si recibe un cambio, debe guardarlo en profFilter
		changeFriend: Si recibe un cambio, debe guardarlo en friendFilter
	FilterPipe
		Transform: Si recibe Null, debe devolver vacío
		Transform: filtro profesión - Empty values
		Transform: filtro profesión - 1 valor
		Transform: filtro friends - Empty values
		Transform: filtro friends - 1 valor
	BrastlewarkDataService
		Debe ser Creado

Para realizar las pruebas, se debe ejecutar 'ng test'.

## Built With
	Angular CLI: 6.0.8
	Node: 8.11.2
	Angular: 6.0.4
	@angular-devkit/architect         0.6.8
	@angular-devkit/build-angular     0.6.8
	@angular-devkit/build-optimizer   0.6.8
	@angular-devkit/core              0.6.8
	@angular-devkit/schematics        0.6.8
	@angular/cli                      6.0.8
	@ngtools/webpack                  6.0.8
	@schematics/angular               0.6.8
	@schematics/update                0.6.8
	rxjs                              6.2.1
	typescript                        2.7.2
	webpack                           4.8.3

## Authors

Devoto Federico 

